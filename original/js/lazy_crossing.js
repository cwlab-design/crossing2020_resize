//Header&Footer
$(".main-header__logo,.main-footer img").addClass("lazyload");

//index
$(".page.index .card__img img,.index-feature__img img,.index-magazine__img img,.instagram--box iframe,.index-magazine,.index-video__content iframe").addClass("lazyload");

//文章內頁
$(".article-box__main .main-article article img,.article-author__img img,.article-interest .card .card__img img,.article-video iframe,.article-shortcut .item a").addClass("lazyload");

//主頻道列表
$(".page.channel .channel-catalog:nth-child(n+4) .card__img img").addClass("lazyload");

//子頻道列表
$(".page.sub-channel .card-group .card:nth-child(n+4) .card__img img").addClass("lazyload");

//策展首頁列表
$(".page.channel-feature .card-group .card__img img").addClass("lazyload");

//策展內頁
$(".page.feature .card .card__img img,.feature-box--slide img,.feature-box--video iframe,.feature-box--features img").addClass("lazyload");

//搜尋結果
$(".result-list .card:nth-child(n+3) .card__img img").addClass("lazyload");

//最新季刊
$(".magazine-list .list-data__content .card__img img").addClass("lazyload");

//作者列表
$(".author-list .card .card__img img").addClass("lazyload");